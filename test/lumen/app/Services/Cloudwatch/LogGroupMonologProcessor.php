<?php

namespace App\Services\Cloudwatch;

use Monolog\LogRecord;

class LogGroupMonologProcessor
{
    public function __invoke(LogRecord $record): LogRecord
    {
        $record->extra['cloudwatch_log_group'] = 'test_group';
        $record->extra['cloudwatch_log_stream'] = 'test_stream';

        // This doesn't get new lines working either.
        // $record['message'] = str_replace(["\r\n", "\n"], ["\r","\r"], $record['message']);

        return $record;
    }
}
