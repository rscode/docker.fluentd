<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Psr\Log\LoggerInterface;

class LogTest extends Command
{
    protected $signature = 'app:log-test {--e|exception : Log a test exception}';

    public function handle(LoggerInterface $logger)
    {
        if ($this->option('exception')) {
            $logger->error(new \Exception('this is a test exception'));
        } else {
            $logger->info('This is a message');
        }
    }
}
