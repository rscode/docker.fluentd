REPO=ratespecial/fluentd

.PHONY: build push test all

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := all

build: ## Build image
	docker build -t ${REPO} .

push: ## Push image to docker hub
	docker push ${REPO}

test: ## Start fluent service using docker-compose
	docker-compose down
	docker-compose up fluent

all: build push ## Build and push