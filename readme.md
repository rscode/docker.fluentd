# Ratespecial fluentd image

Source of docker image: `ratespecial/fluentd`

This image is meant to run in a docker swarm with many Laravel projects.  Each project can specify what Cloudwatch log group and log 
stream to record to.

![diagram](assets/diagram.jfif)

For sending from laravel/lumen to fluent, this package was used: https://github.com/ytake/Laravel-FluentLogger

For sending from fluent to Cloudwatch, this fluentd plugin was used: https://github.com/fluent-plugins-nursery/fluent-plugin-cloudwatch-logs

# How it works

`ratespecial/fluentd` docker image [is configured](docker/fluentd/etc/fluent.conf) to match `laravel.**` tags.  With the help of the 
`ytake/Laravel-FluentLogger` package we can set this.  In that libraries' configuration, make sure the `tagFormat` in the config file is 
someting like `laravel.{{channel}}.{{level_name}}`.
[See the test project for an example](test/lumen/config/fluent.php).

All log messages **MUST** have the following variables in the `extra` array of the log message:

1. `cloudwatch_log_group`
2. `cloudwatch_log_stream`

The fluent filters will look for these to know where to send the message.  The best way to do this is using a Monolog Processor.  The 
`ytake/Laravel-FluentLogger` library can be configured with a processor.  See the test project [config file](test/lumen/config/fluent.php) 
and [processor class](test/lumen/app/Services/Cloudwatch/LogGroupMonologProcessor.php).

# Usage

## Fluent container

The `ratespecial/fluentd` container must have the following environment variables:

```dotenv
AWS_REGION=
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
```

These credentials must have access to write to whatever Cloudwatch group you wish to send to.

## Laravel setup

1. `composer require ytake/laravel-fluent-logger:dev-main`
2. Follow instructions at https://github.com/ytake/Laravel-FluentLogger
2. You must also include the `LogGroupMonologProcessor` in the configuration.  Add this to the `fluent.php` config:

```
'processors' => [new \App\Services\Cloudwatch\LogGroupMonologProcessor()],
```

5. Tags on fluent messages must start with `laravel.`.  Change the fluent.php configuration as follows:

```
'tagFormat' => 'laravel.{{channel}}.{{level_name}}',
```

## A note on building the Cloudwatch plugin

When building on 2023-02-28, I received the following error:

```
ERROR:  Error installing fluent-plugin-cloudwatch-logs:
	The last version of bundler (>= 0) to support your Ruby & RubyGems was 2.3.26. Try installing it with `gem install bundler -v 2.3.26` and then running the current command again
	bundler requires Ruby version >= 2.6.0. The current ruby version is 2.5.0.
```

I added the following line to the `Dockerfile` which fixed it:

```
    && sudo gem install bundler -v 2.3.26 \
```

[It looks like they are up to version 2.4.7](https://rubygems.org/gems/bundler/versions/2.3.26), but that requires >= Ruby 2.6.  I predict we can remove this line sometime in the future.

## Testing

### Setup

Copy `./env-example` to `./.env` and fill out the `AWS_*` environment variables.  These credentials will need access to write to
Cloudwatch.

Copy `./test/lumen/.env-example` to `./test/lumen/.env` so Lumen is happy.

### Testing the fluentd configuration

The Dockerfile is setup to get the fluentd container running.  This will copy in the configuration `docker/fluentd/etc/fluent.conf` into
the image.

You can use `docker-compose` to test it:

`docker-compose up`

You can also use the make file to set it up:

`make test`

Both of these options will bring up the fluentd container.

### Sending a log from lumen to fluentd

Under the `test` folder there is a lumen installation, complete with a fluentd logging package.  To send a log record from lumen
to the fluent container, execute the following:

`docker-compose run --rm laravel php artisan app:log`

### Sending a log from inside the container to fluentd

You can exec into the container using a shell:

`docker exec -it xxx ash`

and send a log directly to fluend.  This sends it under the tag name `laravel.test`:

`echo '{"hello":"world","extra":{"cloudwatch_log_group":"test_group","cloudwatch_log_stream":"test_stream"}}' | fluent-cat laravel.test`

### Output incoming laravel logs to stdout

Uncomment the `<match laravel.**>` block with `@type stdout`.

**Explanation**

There are two `<match laravel.**>` blocks within the config file.  Only the first one fluent reads will be used.  The `@type stdout`
block is before the `@type cloudwatch_logs` block, but commented out.  
