FROM fluent/fluentd

# Use root account to use apk
USER root

RUN apk add --no-cache --update --virtual .build-deps \
            sudo build-base ruby-dev \
     && sudo gem install bundler -v 2.3.26 \
     && sudo gem install fluent-plugin-cloudwatch-logs \
     && sudo gem sources --clear-all \
     && apk del .build-deps \
     && rm -rf /tmp/* /var/tmp/* /usr/lib/ruby/gems/*/cache/*.gem

COPY docker /
